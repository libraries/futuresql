-- SPDX-FileCopyrightText: 2023 Jonah Brüchert <jbb@kaidan.im
--
-- SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
DROP TABLE test;
